module.exports = {
	base: '/my-vuepress/',
	dest: 'public',
	locales: {
		'/': {
			lang: 'en-US',
			title: 'Miss Li\'s Docs',
			description: 'Miss Li\'s Blog'
		},
		'/zh/': {
			lang: 'zh-CN',
			title: '李未的docs',
			description: '李未的博客'
		}
	},
	// 额外的需要被注入到当前页面的 HTML <head> 中的标签
	head: [
		['link', { rel: 'icon', href: './images/logo.jpg' }]
	],
	themeConfig: {
		smoothScroll: true,
		logo: '/assets/img/logo.png',
		locales: {
			'/zh/': {
				label: '简体中文',
				selectText: '选择语言',
				// ariaLabel: '选择语言',
				// 导航栏
				nav: require('./nav/zh'),
				// 侧边栏
				sidebar: {
					'/zh/guide/': getGuideSidebar('指南', 'group2'),
					'/zh/vue/': getVueSidebar(),
					'/zh/react/': getReactSidebar(),
					'/zh/es6/': getEs6Sidebar(),
					'/zh/html/': getHtmlSidebar(),
					'/zh/git/': getGitSidebar(),
				}
			}
		}
	}
}

function getGuideSidebar(groupA, groupB) {
	return [
		{
			title: groupA,
			collapse: false,
			children: [
					'',
					'getting-started',
					'howto-named-project',
					'use-markdown'
			]
		},
		{
			title: groupB,
			collapse: false,
			children: [
				'permalinks',
			]
		},
	]
}
function getVueSidebar() {
	return [
		{
			title: 'vue',
			collapse: false,
			children: [
				'',
				'about'
			]
		}
	]
}
function getReactSidebar() {
	return [
		{
			title: 'react',
			collapse: false,
			children: [
				'',
			]
		}
	]
}
function getEs6Sidebar() {
	return [
		{
			title: 'es6',
			collapse: false,
			children: [
				'',
			]
		}
	]
}
function getHtmlSidebar() {
	return [
		{
			title: 'html',
			collapse: false,
			children: [
				'',
			]
		}
	]
}
function getGitSidebar() {
	return [
		{
			title: 'Git',
			collapse: false,
			children: [
				'',
			]
		}
	]
}