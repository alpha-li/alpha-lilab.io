# 项目部署

| gitlab pages 类型 | gitlab项目名称             | 网址                                                      |
|:---------------:|:----------------------:|:-------------------------------------------------------:|
| 用户页             | username\.example\.io  | http\(s\)://username\.example\.io                       |
| group页          | groupname\.example\.io | http\(s\)://groupname\.example\.io                      |
| 用户独有项目页         | projectname            | http\(s\)://username\.example\.io/projectname           |
| group独有项目页      | projectname            | http\(s\)://groupname\.example\.io/projectname          |
| subgroup独有项目页   | subgroup/projectname   | http\(s\)://groupname\.example\.io/subgroup/projectname |

##### 用户页
* 新建名为 username.gitlab.io 的gitlab仓库

##### 用户独有项目页
* 新建一个 projectname 仓库
* .vuepress中的config.js配置 base: '/projectname/',

