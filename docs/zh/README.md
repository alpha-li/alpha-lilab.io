---
home: true
heroImage: /assets/img/logo.jpg
heroText: null
tagline: null
actionText: 点击进入 →
actionLink: /zh/guide/
features:
- title: 你好呀
  details: 这是我的文章集。
- title: 很开心见到你
  details: 开始自己的技术记录
- title: Fighting！
  details: 加油加油加油！
footer: Miss Li 自己的网站
---