module.exports = [
  {text: '主页', link: '/'},
  {text: '指南', link: '/zh/guide/'},
  {
    text: '博文',
    items: [
      {text: 'vue', link: '/zh/vue/'},
      {text: 'react', link: '/zh/react/'},
      {text: 'html', link: '/zh/html/'},
      {text: 'es6', link: '/zh/es6/'},
      {text: 'Git', link: '/zh/git/'}
    ]
  },
  {text: '扩展', link: 'https://google.com'},
]
