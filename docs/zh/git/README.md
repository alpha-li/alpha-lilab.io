# Git学习

<img src="https://user-gold-cdn.xitu.io/2020/6/25/172ea48acc9e10d9?imageslim">

### 保存临时工作成果Stash
#### git stash
#### git stash pop
在你需要并行做好几件事的时候，
比如你正在开发一个功能程序开发到一半，
有一个紧急的bug需要处理又或者你突然对另外一个新特性来了思路，
但是你现有的代码还在一个中间状态甚至编译都有问题。
你需要可定需要一个功能保存现在的工作现场，然后去干另外一件事。
这个时候stash功能就可以帮助你解决

***

### 版本回退
#### git reset（--mixed） HEAD^
只是版本回退 不更新工作区，difference 不会在暂存区，需要手动add
#### git reset --hard HEAD^
不但版本回退 也会更新工作区（文件目录）的文件到上一个版本
#### git reset --soft HEAD^
工作区文件不会有变化，difference 会在暂存区等待提交

*** 

### 清洗提交历史 
#### git merge --squash
希望通过一次的commit来保留中间成果，最终提交的时候不希望中间细节被被人了解。
该操作是把合并分支的最终代码暂存到缓存区，通过在master主分支上进行一次commit提交来实现效果。

***

### 改变分支依赖 
#### git rebase 
合并多次提交记录 git rebase -i HEAD~4

    p, pick = use commit
    r, reword = use commit, but edit the commit message
    e, edit = use commit, but stop for amending
    s, squash = use commit, but meld into previous commit
    f, fixup = like “squash”, but discard this commit’s log message
    x, exec = run command (the rest of the line) using shell
    d, drop = remove commit
 
* 本地与远端同一分支提交历史不一致
* 不同分支之间的合并
<br>`https://www.jianshu.com/p/f7ed3dd0d2d8`